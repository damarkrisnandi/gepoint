import Vue from 'vue'
import Router from 'vue-router'

import adminLogin from '@/components/adminLogin'
import dashboard from '@/components/dashboardPage'

import hospitalList from '@/components/dashboard-mainpanel/hospital/dbHospitalList'
import editHospital from  '@/components/dashboard-mainpanel/hospital/dbEditHospital'
import createHospital from  '@/components/dashboard-mainpanel/hospital/dbAddHospital'

import createDeals from '@/components/dashboard-mainpanel/deals/createDeals'
import homeDeals from '@/components/dashboard-mainpanel/deals/homeDeals'
import editDeals from '@/components/dashboard-mainpanel/deals/editDeals'
import dealsList from '@/components/dashboard-mainpanel/deals/dealsList'

import createProducts from '@/components/dashboard-mainpanel/products/createProducts'
import homeProducts from '@/components/dashboard-mainpanel/products/homeProducts'
import editProducts from '@/components/dashboard-mainpanel/products/editProducts'
import productList from '@/components/dashboard-mainpanel/products/productList'

import createNews from '@/components/dashboard-mainpanel/news/dbCreateNews'
import newsList from '@/components/dashboard-mainpanel/news/dbNewsList'
import editNews from '@/components/dashboard-mainpanel/news/dbEditNews'

Vue.use(Router)


export default new Router({
  routes: [
    {
      path: '/',
      name: 'adminLogin',
      component: adminLogin
    },
    {
      path: '/admin',
      name: 'dashboardPage',
      component: dashboard,
      children: [
    //hospital    
        {
            path: '/admin/hospital',
            name: 'dbHospitalList',
            component: hospitalList,
        },
        {
          path: '/admin/hospital/create',
          name: 'dbCreateHospital',
          component: createHospital
        },
        {
          path: '/admin/hospital/edit',
          name: 'dbEditHospital',
          component: editHospital
        },

        //products
        {
          path: '/admin/products/create',
          name: 'createProducts',
          component: createProducts
        },
        {
          path: '/admin/products/edit',
          name: 'editProducts',
          component: editProducts
        },
        {
          path: '/admin/products',
          name: 'homeProducts',
          component: homeProducts
        },
        {
          path: '/admin/products/list',
          name: 'productList',
          component: productList
        },

        //deals
        {
          path: '/admin/deals/create',
          name: 'createDeals',
          component: createDeals
        },
        {
          path: '/admin/deals/edit',
          name: 'editDeals',
          component: editDeals
        },
        {
          path: '/admin/deals',
          name: 'homeDeals',
          component: homeDeals
        },
        {
          path: '/admin/deals/list',
          name: 'dealsList',
          component: dealsList
        },

        //news
        {
          path: '/admin/news/create',
          name: 'dbCreateNews',
          component: createNews
        },
        {
          path: '/admin/news',
          name: 'dbNewsList',
          component: newsList
        },
        {
          path: '/admin/news/edit',
          name: 'dbEditNews',
          component: editNews
        },
        ]
    }
  ]
})
